from fileinput import filename
import os, sys
from pytube import YouTube
from datetime import datetime
from moviepy.editor import VideoFileClip


def videoDL(youtube_url):
    vid = YouTube(youtube_url)
    try:
        filters = vid.streams.filter(progressive=True, file_extension='mp4')
        file_name = (vid.title).replace(' ', '-')+'.mp4'
        date = datetime.today().strftime("%d%m%Y")
        filters.get_highest_resolution().download(output_path='/Users/Auger/Videos/youtube-dump', filename=file_name, filename_prefix=date)
        print('Video downloaded sucessfully :)')
        return '/Users/Auger/Videos/youtube-dump/'+ date + file_name
    except Exception as err:
        print('Error: ',err)

# Takes a filepath to a video and writes its audio to a wav file
def audioExtract(file_name):
    # Change this to change the ouput type (eg. mp3, wav)
    output_extension = 'wav'
    name, ext = os.path.splitext(file_name)
    clip = VideoFileClip(file_name)
    clip.audio.write_audiofile(f"{name}.{output_extension}")

if __name__ == '__main__':
    youtube_url = sys.argv[1].strip()
    print(youtube_url)
    audioExtract(videoDL(youtube_url))

